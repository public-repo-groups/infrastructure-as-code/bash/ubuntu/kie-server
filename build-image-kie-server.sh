#!/bin/bash
echo "Start build docker image"

docker build -t dev-kie-server:7.65.0.Final .

echo "End build docker image"

echo "Start docker compose up"

docker-compose up -d

echo "End docker compose up"
